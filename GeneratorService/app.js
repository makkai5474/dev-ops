const express = require("express");
const winston = require("winston");
const Elasticsearch = require("winston-elasticsearch");
const app = express();
// const Logstash = require("logstash-tcp-wins");

const port = (process.env.PORT = 4050);

const esTransportOpts = {
  level: "info",
  clientOpts: {
    host: "http://elk.apps.okd.codespring.ro",
    log: "info"
  },
  indexPrefix: "mattila"
};

const esLogger = winston.createLogger({
  transports: [new Elasticsearch(esTransportOpts)]
});
// const logstashLogger = winston.createLogger({
//   transports: [
//     new Logstash({
//       level: "info",
//       port: 5000,
//       host: "localhost"
//     })
//   ]
// });

app.get("/", (req, res) => {
  const rn = randomNumberUpTo(50);

  esLogger.info(`Random number for es: ${rn}`);
  // logstashLogger.info(`Random number for logstash: ${rn}`);

  res.send(`Random number: ${rn}`);
});

app.listen(port, () => {
  console.log(`server is running on http://localhost:${port}`);
});

function randomNumberUpTo(max) {
  return Math.floor(Math.random() * max);
}
